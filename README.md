lily-testkit
============

Unit testing for Lily libraries.

This library provides `testkit`, a system for running unit testing on a Lily
library.

There is currently no proper documentation of the api for the `testing` module
or `testkit` itself. This is due in part to documentation generation not yet
working properly for native libraries.

This can be installed using Lily's `garden` via:

`garden install FascinatedBox/lily-testkit`

### Test Design

For a package named `potato`, the interpreter expects that the main driving file
is located at `src/potato.{suffix}`. Testkit tests are located in a `test/`
directory that is situated alongside the `src` directory. Testkit begins by
attempting to import `test/__test__.lily`.

The driving test file `test/__test__.lily` is allowed to import other test files
(which must start with `test_`) as well as files within the `src` directory.
Inside testkit, this works by creating a subinterpreter and overriding the
default importing behavior.

Because testkit uses a subinterpreter and overrides the default import
mechanism, testing can be done on both foreign and native packages.

### Creating Tests

Start by calling `import testing` from any test file. Testing classes are any
class that inherits from `testing.TestCase`. Testing classes should be written
to not take any arguments in their constructor. Within a testing class, only
methods that are public and have a name starting with `test_` are considered
test method candidates. All other methods will not be executed by the testing
system.

Testing files are permitted to define non-testing helper methods in testing
classes, as well as ordinary, non-testing classes. However, testing files should
not initialize any testing code. Testkit comes with logic that allows it to
select particular classes and tests to be run.

The contents of the `testing` module are detailed in `README_testing.md`.
