testing
=======

This file documents the api provided by the `testing` module that is provided to
the testing system.

```
# Exception raised to signal an assert has failed.
class TestAssertionFailed(message: String)

# Exception raised to signal a test has been skipped.
class TestSkipped(message: String) < Exception(message)

class TestCase
    # Raise TestAssertionFailed with 'left' and 'right' if they are not equal.
    public define assert_equal[A](left: A, right: A) {

    # Raise TestAssertionFailed if 'x' and 'y' are not within 'delta' range.
    public define assert_near_equal(x: Double,
                                    y: Double,
                                    delta: *Double=0.0000001)

    # Raise TestAssertionFailed if 'left' and 'right' are equal.
    public define assert_not_equal[A](left: A, right: A)

    # Raise TestAssertionFailed if 'value' is true.
    public define assert_false(value: Boolean)

    # Raise TestAssertionFailed if 'value' is false.
    public define assert_true(value: Boolean)

    # Raise TestAssertionFailed if 'fn' does not raise an exception named 'except'.
    public define assert_raises(expect: String, fn: Function())

    # Raise TestAssertionFailed with 'reason'.
    public define fail(reason: String)

    # Raise TestSkipped with 'reason'.
    public define skip(reason: String)
```
