import (Interpreter) spawni

define push_if_missing[A](source: List[A], value: A)
{
    var found = false

    for i in 0...source.size() - 1: {
        var v = source[i]
        if v == value: {
            found = true
            break
        }
    }

    if found == false: {
        source.push(value)
    }
}

define full_import(interp: Interpreter, dirname: String, target: String)
{
    interp.import_use_local_dir(dirname)
    if interp.import_file(target) ||
       interp.import_library(target): {
        return
    }

    interp.import_use_package_dir(dirname)
    if interp.import_file(target) ||
       interp.import_library(target): {
        return
    }
}

scoped enum TestResult
{
    Failed(:name String, :message String, :trace String),
    Passed,
    Skipped(:reason String)

    define as_string: String
    {
        match self: {
            case TestResult.Passed:
                return "ok"
            case TestResult.Skipped(reason):
                return "skipped '{0}'".format(reason)
            case TestResult.Failed(name, message, trace):
                return "failed\n{0}: {1}\n{2}\n".format(name, message, trace)
        }
    }

    define is_failed: Boolean
    {
        match self: {
            case TestResult.Failed(_, _, _):
                return true
            else:
                return false
        }
    }

    define is_passed: Boolean
    {
        match self: {
            case TestResult.Passed:
                return true
            else:
                return false
        }
    }

    define is_skipped: Boolean
    {
        match self: {
            case TestResult.Skipped(_):
                return true
            else:
                return false
        }
    }
}

class TestTarget(public var @module_name: String,
                 public var @class_name: String,
                 public var @method_name: String)
{
    public var @path = @module_name ++ "." ++ @class_name ++ "." ++ @method_name
}

class TestKitInterpreter < Interpreter
{
    private var @dirname = ""

    private define import_root_file
    {
        # Import a bootstrap to get testing to run.
        import_use_local_dir("")

        # Import the runner and the catalog here so the launcher has a
        # consistent place to source them from.
        import_string("__init__", """
            import __test__
            import (TestRunner) testing
            import (get_catalog) catalog
        """)
    }

    private define do_import_hook(target: String)
    {
        var root_dir = import_current_root_dir()

        if target == "catalog" || target == "testing": {
            var dir = __dir__.replace("\/", "/")

            import_use_local_dir(dir ++ "subinterp")
            import_file(target)
            return
        }

        if target.ends_with("__init__"): {
            import_root_file()
        elif root_dir == "":
            if target.starts_with("test_"): {
                import_use_local_dir(@dirname ++ "test")
                import_file(target)
            elif target != "__test__":
                full_import(self, @dirname ++ "src", target)
            else:
                # __test__ is going to be a file in the test dir.
                import_use_local_dir(@dirname ++ "test")
                import_file(target)
            }
        else:
            full_import(self, root_dir, target)
        }
    }

    private static define hook(s: Interpreter, target: String)
    {
        match s: {
            case TestKitInterpreter(i):
                TestKitInterpreter.do_import_hook(i, target)
            else:
        }
    }

    public define expr_for_message(to_send: String): Option[String]
    {
        var run_result = parse_expr("[test]", to_send)
        var result: Option[String] = None

        match run_result: {
            case Some(s):
                var output = s.split(":")
                              .slice(1)
                              .join(":")
                              .strip(" ")

                # If output is a string, it's quoted. Remove the quotes.
                if output.starts_with("\""): {
                    output = output.slice(1, -1)
                }

                result = Some(output)
            case None:
                print(error())
        }

        return result
    }

    public define parse_for_result(to_send: String): Result[String, Unit]
    {
        var ok = parse_string("[test]", to_send)

        if ok: {
            return Success(unit)
        else:
            return Failure(error())
        }
    }

    public define get_target_catalog: String
    {
        return expr_for_message("__init__.get_catalog()").unwrap()
    }

    public define first_pass(dirname_str: String): Result[String, Unit]
    {
        if dirname_str == "": {
            return Failure("Cannot select an empty dirname.")
        elif @dirname != "":
            return Failure("Cannot retry after a failure.")
        }

        @dirname = dirname_str.replace("\/", "/")

        set_hook(hook)

        # Protect against quote injection.
        var safe_dirname = @dirname.replace("\"", "\\\"")

        var boot_line = """
            import "{0}__init__"

            var r = __init__.TestRunner()
        """.format(safe_dirname)

        var ok = parse_string("[test]", boot_line)
        var out: Result[String, Unit] = Success(unit)

        if ok == false: {
            out = Failure(error())
        }

        return out
    }
}

class TestBase
{
    public var @name = ""
    public var @prefix = ""
    public var @children: List[TestBase] = []

    public define add(to_add: TestBase)
    {
        @children.push(to_add)
    }

    public define select(name: String): TestBase
    {
        var result = @children.select(|s| s.name == name )
                              .get(0)
                              .unwrap_or(TestBase())

        return result
    }

    public define all: List[TestBase]
    {
        return @children
    }
}

class TestModule < TestBase {}

class TestClass < TestBase
{
    public var @instance_name = ""
    public var @loaded = false
}

class TestMethod(public var @parent: TestClass) < TestBase {}

class TestKit
{
    private var @interp = TestKitInterpreter()
    private var @test_root = TestModule()
    private var @method_bucket: Hash[String, TestMethod] = []

    private define target_for_path(path: String): TestBase
    {
        var item: TestBase = @test_root

        # Special case an empty path to return the root (all tests).
        if path == "": {
            return item
        }

        var split_path = path.split(".")

        for i in 0...split_path.size() - 1: {
            item = item.select(split_path[i])
        }

        return item
    }

    private define parse_catalog(catalog: String)
    {
        var catalog_lines = catalog.trim().split("\n")
        var class_counter = 0
 
        # The first module to come through is the root.
        var current_module = @test_root
        var current_class = TestClass()
        var prefix = ""

        var module_bucket: Hash[String, TestModule] = []

        for i in 0...catalog_lines.size() - 1: {
            var split_line = catalog_lines[i].split("|")

            var command = split_line[0]
            var name = split_line[1]

            if command == "module": {
                prefix = split_line[2]

                if current_module.name != "": {
                    # The path provided is for the parent.
                    var bucket_prefix = prefix

                    current_module = TestModule()
                    prefix = prefix ++ "." ++ name

                    # Add child to parent (from bucket)
                    module_bucket[bucket_prefix].add(current_module)
                else:
                    @test_root = current_module
                }

                # Add the child to the bucket.
                module_bucket[prefix] = current_module

                current_module.name = name
                current_module.prefix = prefix
            elif command == "class":
                if current_class.name != "": {
                    current_class = TestClass()
                }

                current_class.name = name
                current_class.prefix = prefix
                current_class.instance_name = "c" ++ class_counter

                class_counter += 1

                current_module.add(current_class)
            elif command == "method":
                var m = TestMethod(current_class)

                m.name = name
                m.prefix = prefix

                current_class.add(m)
            }
        }
    }

    private define load_class(target: TestClass): Boolean
    {
        var bootcode = """\
            var {0} = __init__.{1}.{2}()\
        """.format(target.instance_name,
                   target.prefix,
                   target.name)

        var load_result = @interp.parse_for_result(bootcode)
        var ok = load_result.is_success()

        target.loaded = ok

        return ok
    }

    private define walk_target(source: TestBase): List[TestTarget]
    {
        var walk_queue: List[List[TestBase]] = []
        var walk_list = [source]
        var targets: List[TestTarget] = []
        var classes: List[TestClass] = []

        # In every other case, a method is only found inside of a class, and the
        # class case makes sure the class is loaded.
        match source: {
            case TestMethod(m):
                push_if_missing(classes, m.parent)
            else:
        }

        while 1: {
            for i in 0...walk_list.size() - 1: {
                match walk_list[i]: {
                    case TestModule(m):
                        m.all() |> walk_queue.push
                    case TestClass(c):
                        push_if_missing(classes, c)
                        c.all() |> walk_queue.push
                    case TestMethod(m):
                        var t = TestTarget(m.prefix, m.parent.name, m.name)

                        @method_bucket[t.path] = m
                        targets.push(t)
                    else:
                }
            }

            if walk_queue.size(): {
                # Use shift to preserve first-in-first-out order.
                # Otherwise picking a module runs classes in reverse decl order.
                walk_list = walk_queue.shift()
            else:
                break
            }
        }

        for i in 0...classes.size() - 1: {
            var c = classes[i]
            var load_result = load_class(c)

            if load_result == false: {
                targets = []
                break
            }
        }

        return targets
    }

    public define load_tests(dirname: String): Result[String, Unit]
    {
        var out: Result[String, Unit] = Success(unit)

        if @test_root.name == "": {
            match @interp.first_pass(dirname): {
                case Success(_):
                    @interp.get_target_catalog() |> parse_catalog
                case Failure(f):
                    out = Failure(f)
            }
        else:
            out = Failure("Tests have already been loaded.")
        }

        return out
    }

    public define select_target(path: String): List[TestTarget]
    {
        return target_for_path(path)
                |> walk_target
    }

    public define select_all: List[TestTarget]
    {
        return @test_root |> walk_target
    }

    public define run_target(target: TestTarget): TestResult
    {
        var m = @method_bucket[target.path]

        var bootcode = """\
            r.run({0}, __init__.{1}, "{2}")\
        """.format(m.parent.instance_name,
                   target.path,
                   m.name)

        var message = @interp.expr_for_message(bootcode)
                             .unwrap()
                             .split("|")
        var expr_result = message[0]
        var out = TestResult.Passed

        if expr_result == "skipped": {
            out = TestResult.Skipped(:reason message[1])
        elif expr_result == "failed":
            out = TestResult.Failed(:name    message[1],
                                    :message message[2],
                                    :trace   message[3])
        }

        return out
    }
}
